#include <stdio.h>
#include "library.h"
#include "date.h"
#include <string.h>
#include<stdlib.h>
void user_accept(USER_T *u)
{
  
	printf("name: ");
	scanf("%s", u->name);
	printf("email: ");
	scanf("%s", u->email);
	printf("phone: ");
	scanf("%s", u->phone);
	printf("password: ");
	scanf("%s", u->passwd);
	if(strcmp(u->email,EMAIL_OWNER)==0)
	strcpy(u->role, ROLE_OWNER);
	else
	strcpy(u->role, ROLE_MEMBER);

}
void user_display(USER_T *u)
{
	printf("======================================================\n");

    printf("id: %d\nname: %s\nemail: %s\nphone: %s\nrole: %s\n", u->id, u->name, u->email, u->phone, u->role);
	printf("======================================================\n");
}

void book_accept(BOOK_T *b)
{
	printf("name: ");
	scanf("%s", b->name);
	printf("author: ");
	scanf("%s", b->author);
	printf("subject: ");
	scanf("%s", b->subject);
	printf("price: ");
	scanf("%lf", &b->price);
	printf("isbn: ");
	scanf("%s", b->isbn);

}
void book_display(BOOK_T *b)
{
		printf("======================================================\n");

    printf("id: %d\nname: %s\nauthor: %s\nsubject: %s\nprice: %.2lf\nisbn: %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
		printf("======================================================\n");

}
void add_copies(BOOKCOPY_T *b)
{

    printf("bookid: \n");
    scanf("%d",&b->bookid);
    printf("rack: \n");
    scanf("%d",&b->rack);
	strcpy(b->status,STATUS_AVAIL);
}
void bookcopy_display(BOOKCOPY_T *c) {
	printf("======================================================\n");

	printf("id: %d\nbookid: %d\nrackNo: %d\nstatus: %s\n", c->id, c->bookid, c->rack, c->status);
	printf("======================================================\n");

}

void issue_book_Accept(ISSUERECORD_T *b)
{
    printf("copyid: \n");
    scanf("%d",&b->copyid);
    printf("memberid: \n");
    scanf("%d",&b->memberid);
    printf("issue_date: \n");
	input_date(&b->issue_date);
	DATE temp;
	temp.dd=b->issue_date.dd;
	temp.mm=b->issue_date.mm;
	temp.yyyy=b->issue_date.yyyy;
	b->return_duedate=increment_date(temp,BOOK_RETURN_DAYS);
    memset(&b->return_date,0,sizeof(DATE));
	b->fine_amount=0.0;
}
void issuerecord_display(ISSUERECORD_T *r) {
	printf("===================================================================\n");
	printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	print_date(&r->issue_date);
	printf("return due ");
	r->return_duedate=increment_date(r->issue_date, BOOK_RETURN_DAYS);
	print_date(&r->return_duedate);
	printf("return ");
	print_date(&r->return_date);
	printf("===================================================================\n");
}
void payment_accept(PAYMENT_T *p)
{

	printf("member id: ");
	scanf("%d", &p->userid);
	/*printf("type (fees/fine): ");
	scanf("%s", p->type);*/
	printf("amount: ");
	scanf("%lf", &p->amount);
	p->transaction_time=date_current() ;
	if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->nextpayment_duedate = increment_date(p->transaction_time, MEMBERSHIP_MONTH_DAYS);
	else
		memset(&p->nextpayment_duedate, 0, sizeof(DATE));
		printf("next payment due date : ");
		print_date(&p->nextpayment_duedate);

}
void payment_display(PAYMENT_T *p) {
	printf("===============================================\n");
	printf("payment id: %d, member id: %d, type: %s, amount: %.2lf\n", p->id, p->userid, p->type, p->amount);
	printf("payment ");
	print_date(&p->transaction_time);
	printf("payment due ");
	print_date(&p->nextpayment_duedate);
	printf("===============================================\n");
}

void sign_up(){
	USER_T u;
	u.id=get_next_user_id();
	user_accept(&u);
	user_add(&u);
}
void user_add(USER_T *u) 
{
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}
	fwrite(u, sizeof(USER_T), 1, fp);
	printf("user added into file.\n");
	fclose(fp);
}
int user_find_by_email(USER_T *u, char email[30]) {
	FILE *fp;
	int found = 0;
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	while(fread(u, sizeof(USER_T), 1, fp) > 0) {
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	
	fclose(fp);

	return found;
}
void sign_in()
{
	char email[30]; char password[10];
	USER_T u;
	int flag=0;
	printf("Enter email: \n");
	scanf("%s",email);
	printf("Enter password: \n");
	scanf("%s", password);
	if(user_find_by_email(&u, email)==1)
	{
		if(strcmp(password,u.passwd)==0)
		{
			if(strcmp(email,EMAIL_OWNER)==0)
			strcpy(u.role,ROLE_OWNER);
			if(strcmp(u.role,ROLE_OWNER)==0)
			owner_area(&u);
			else if(strcmp(u.role,ROLE_LIBRARIAN)==0)
			librarian_area(&u);
			else if(strcmp(u.role,ROLE_MEMBER)==0)
			member_area(&u);
			else 
			flag=1;
		}
		else
		   flag=1;
		
	}
	else
	       flag=1;
	if(flag)
	printf("Inavalid Email/password/role\n");
}
int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(USER_T);
	USER_T u;
	
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;

	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;

	fclose(fp);
	
	return max + 1;
}
int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(BOOK_T);
	BOOK_T u;
	
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;

	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;
	
	fclose(fp);

	return max + 1;
}
void edit_profile(USER_T *u)
{
   int found=0;
   char pass[15];
   printf("password: ");
   scanf("%s",pass);
	FILE *fp;
    USER_T i;
    fp=fopen(USER_DB,"rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while(fread(&i, sizeof(USER_T), 1, fp) > 0) {
		if(strcmp(i.email,u->email)==0) {
			if(strcmp(i.passwd,pass)==0){
            found = 1;
			break;
            }
		}
	}
   if(found) {
		long size = sizeof(USER_T);
		USER_T x;
		if(strcmp(u->role,ROLE_OWNER)!=0)
		{printf("Enter new email: ");
        scanf("%s",x.email);}
		else
		{
		   strcpy(x.email,EMAIL_OWNER);
		}
		
        printf("Enter new phone number: ");
        scanf("%s",x.phone);
        printf("Enter new password: ");
        scanf("%s",x.passwd);
	    x.id = i.id;
       strcpy(x.name,i.name);
	   if(strcmp(u->role,ROLE_LIBRARIAN))
	   {
		   strcpy(x.role,u->role);
	   }
	   else if(strcmp(u->role,ROLE_OWNER)==0)
	   {
		   strcpy(x.role,u->role);
	   }
	   else{
		   strcpy(x.role,i.role);
	   }
      
		fseek(fp, -size, SEEK_CUR);
		fwrite(&x, size, 1, fp);
		printf("profile updated successfully.\n");
	}
	else
    printf("Invalid email/password OR User not found!!\n");
	fclose(fp);
}
void change_password(USER_T *u)
{
	int found = 0;
   char pass[15];
   printf("password: ");
   scanf("%s",pass);
	FILE *fp;
    USER_T i;
    fp=fopen(USER_DB,"rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while(fread(&i, sizeof(USER_T), 1, fp) > 0) {
		if(strcmp(i.email,u->email)==0) {
			if(strcmp(i.passwd,pass)==0){
            found = 1;
			break;
            }
		}
	}
   if(found) {
		long size = sizeof(USER_T);
		USER_T x;
		
     
        printf("Enter new password: ");
        scanf("%s",x.passwd);
	   
		strcpy(x.email,i.email);
		strcpy(x.phone,i.phone);
       strcpy(x.name,i.name);
       strcpy(x.role,i.role);
	    x.id = i.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&x, size, 1, fp);
		printf("password changed successfully.\n");
	}
	else
    printf("Invalid email/password OR User not found!!\n");
	fclose(fp);
	
}
int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(BOOKCOPY_T);
	BOOKCOPY_T u;
	
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	fseek(fp, -size, SEEK_END);
	
	if(fread(&u, size, 1, fp) > 0)
	
		max = u.id;
	
	fclose(fp);
	
	return max + 1;
}
int get_next_issuerecord_id()
{
FILE *fp;
	int max = 0;
	int size = sizeof(ISSUERECORD_T);
	ISSUERECORD_T i;
	
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;

	fseek(fp, -size, SEEK_END);
	
	if(fread(&i, size, 1, fp) > 0)
		
		max = i.id;
	
	fclose(fp);

	return max + 1;
}
int get_next_payment_id()
{
	FILE *fp;
	int max = 0;
	int size = sizeof(PAYMENT_T);
	PAYMENT_T p;
	
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;

	fseek(fp, -size, SEEK_END);
	
	if(fread(&p, size, 1, fp) > 0)
		
		max = p.id;
	
	fclose(fp);

	return max + 1;
}
int payment_history(int id)
{
    FILE *fp;
    PAYMENT_T p;
    fp=fopen(PAYMENT_DB,"rb");
    if(fp == NULL) {
		perror("cannot open payment record file");
		exit(1);
	
    }

    while(fread(&p,sizeof(PAYMENT_T),1,fp)>0)
    {
		
        if(id==p.userid)
        payment_display(&p);
    }
    fclose(fp);
}