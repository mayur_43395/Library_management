#ifndef LIBRARY_H_INCLUDED
#define LIBRARY_H_INCLUDED
#define ROLE_OWNER 		"owner"
#define ROLE_LIBRARIAN 	"librarian"
#define ROLE_MEMBER 	"member"

#define STATUS_AVAIL	"available"
#define STATUS_ISSUED	"issued"

#define PAY_TYPE_FEES	"fees"
#define PAY_TYPE_FINE	"fine"

#define FINE_PER_DAY			5
#define BOOK_RETURN_DAYS		7
#define MEMBERSHIP_MONTH_DAYS	30
#define USER_DB		"users.db"
#define BOOK_DB     "books.db"
#define BOOKCOPY_DB "bookcopy.db"
#define EMAIL_OWNER                "owner@gmail.com"
#define STATUS_AVAIL   "available"
#define STATUS_ISSUED  "issued"
#define ISSUERECORD_DB "issuerecord.db"
#define PAYMENT_DB "payment.db"
#include "date.h"
typedef struct user{
int id;
char name[50];
char email[30];
char phone[11];
char passwd[21];
char role[11];
}USER_T;
typedef struct book{
int id;
char name[50];
char author[30];
char subject[30];
double price;
char isbn[16];
}BOOK_T;
typedef struct bookcopy{
int id;
int bookid;
int rack;
char status[11];
}BOOKCOPY_T;
typedef struct issuerecord{
int id;
int copyid;
int memberid;
DATE issue_date;
DATE return_duedate;
DATE return_date;
double fine_amount;
}ISSUERECORD_T;
typedef struct payment{
    int id;
    int userid;
    double amount;
    char type[10];
    DATE transaction_time;
    DATE nextpayment_duedate;
}PAYMENT_T;
//user functions
void user_accept(USER_T *u);
void user_display(USER_T *u);

//Book function
void book_accept(BOOK_T *b);
void book_display(BOOK_T *b);
void add_copies(BOOKCOPY_T *b);
void bookcopy_display(BOOKCOPY_T *c);
void issue_book_Accept(ISSUERECORD_T *b);
void issuerecord_display(ISSUERECORD_T *r);
//owner functions
void owner_area(USER_T *u);
void appoint_librarian();
void fees_report();
void book_category();
void fine_report();
//librarian functions
void librarian_area(USER_T *u);
void add_book();
void find_book();
void book_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
void bookcopy_issue();
void book_changestatus(int bookcopy_id, char status[]);
void display_issued_bookcopies(int member_id);
void bookcopy_return();
void change_rack();
void list_issued_books();
void list_all_members();
void take_payment();
int is_paid_user(int memberid);
void fine_payment(int memberid, double fine_amount);
//member functions
void member_area(USER_T *u);
void bookcopy_checkavail();
void display_issued_books(int id);
//common functions
void sign_in();
void sign_up();
void edit_profile(USER_T *u);
void change_password(USER_T *u);
void user_add(USER_T *u) ;
int user_find_by_email(USER_T *u, char email[]);
void payment_accept(PAYMENT_T *p);
void payment_display(PAYMENT_T *p);
int get_next_user_id();
int get_next_book_id();
int get_next_bookcopy_id();
int get_next_issuerecord_id();
int get_next_payment_id();
int payment_history(int id);


#endif // LIBRARY_H_INCLUDED
