#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(USER_T *u)
{
    int choice;
    do
    {
       printf("\n\n0-sign out\n1-edit profile\n2-change password\n3-find book by name\n4-check book availability\n5-list issued books\n6-payment history\nenter choice: \n");
       scanf("%d",&choice);
       switch(choice)
       {
        case 1:edit_profile(u);break;
        case 2:change_password(u); break;
        case 3:find_book(); break;
        case 4:bookcopy_checkavail();break;
        case 5:display_issued_books(u->id);break;
        case 6:payment_history(u->id);break;
       }
    } while (choice!=0);
    
}
void bookcopy_checkavail()
{
    int book_id;
    FILE *fp;
    BOOKCOPY_T c;
    int count=0;
    printf("enter book id: ");
    scanf("%d",&book_id);
    fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}
    while(fread(&c,sizeof(BOOKCOPY_T), 1,fp)>0)
    {
        if(c.bookid==book_id && strcmp(c.status,STATUS_AVAIL)==0)
        {
            count++;
        }
    }
    fclose(fp);
    if(count==0)
    printf("No copies available currently.\n");
    else
    printf("Number of copies available are: %d\n",count);
    
}
void display_issued_books(int id)
{
    FILE *fp;
    ISSUERECORD_T i;
    fp=fopen(ISSUERECORD_DB,"rb");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while(fread(&i,sizeof(ISSUERECORD_T),1,fp)>0){
        if(i.memberid==id)
        issuerecord_display(&i);
    }
   
    fclose(fp);
}