#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void librarian_area(USER_T *u)
{
    int choice,id;
    do{
        printf("\n\n0-Sign out\n1-edit profile\n2-change password\n3-find book by name\n4-check book availability\n5-add new book\n6-add new copy\n7-issue book copy\n8-return book copy\n9-list issued books\n10-edit book\n11-change rack\n12-add member\n13-take payment\n14-payment history\n15-Display all members\nenter choice: \n");
        scanf("%d",&choice);
        switch(choice)
        {
        case 1:edit_profile(u);break;
        case 2:change_password(u);break;
        case 3:find_book();break;
        case 4:bookcopy_checkavail_details(); break;
        case 5:add_book(); break;
        case 6:bookcopy_add(); break;
        case 7:bookcopy_issue(); break;
        case 8:bookcopy_return();break;
        case 9:list_issued_books();break;
        case 10: book_edit_by_id();break;
        case 11:change_rack();break;
        case 12:sign_up();break;
        case 13:take_payment();break;
        case 14:
                printf("User id: ");
                scanf("%d",&id);
                payment_history(id);break;
        case 15:list_all_members(); break;
        }
    }while(choice!=0);

}
void add_book()
{
    BOOK_T b;
    b.id=get_next_book_id();
    book_accept(&b);
    FILE *fp;
    fp=fopen(BOOK_DB,"ab");
    if(fp==NULL)
    {
        perror("failed to open users file");
		return;
    }
    fwrite(&b, sizeof(BOOK_T), 1, fp);
	printf("Book added into file.\n");
	fclose(fp);
}
void find_book()
{
    char bname[30];
    int flag=0;
    BOOK_T b;
    printf("Book name: \n");
    scanf("%s",bname);
    FILE *fp;
    fp=fopen(BOOK_DB,"rb");
    if(fp == NULL) {
		perror("failed to open users file");
		return;
	}while(fread(&b, sizeof(BOOK_T), 1, fp) > 0) {
		if(strstr(b.name, bname)) {
			flag = 1;
			book_display(&b);
		}
	}
    
}
void book_edit_by_id()
{
    int id, found = 0;
	FILE *fp;
	BOOK_T b;
    printf("Enter book id: ");
    scanf("%d",&id);
    fp=fopen(BOOK_DB,"rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while(fread(&b, sizeof(BOOK_T), 1, fp) > 0) {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
  if(found) {
		long size = sizeof(BOOK_T);
		BOOK_T nb;
		book_accept(&nb);
		nb.id = b.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
	}
  else 
		printf("Book not found.\n");
	
	fclose(fp);
}

void bookcopy_add() {
	FILE *fp;
	BOOKCOPY_T b;
	add_copies(&b);
	b.id = get_next_bookcopy_id();
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	
    }
	fwrite(&b, sizeof(BOOKCOPY_T), 1, fp);
	printf("book copy added in file.\n");
   fclose(fp);
}
void  bookcopy_checkavail_details()
{
    int book_id;
    FILE *fp;
    BOOKCOPY_T c;
    int count=0;
    printf("enter book id: ");
    scanf("%d",&book_id);
    fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}
    while(fread(&c,sizeof(BOOKCOPY_T), 1,fp)>0)
    {
        if(c.bookid==book_id && strcmp(c.status,STATUS_AVAIL)==0)
        {
            bookcopy_display(&c);
            count++;
        }
    }
    fclose(fp);
    if(count==0)
    printf("No copies available currently.\n");
}
void bookcopy_issue()
{
    ISSUERECORD_T rec;
    FILE *fp;
    issue_book_Accept(&rec);
    if(!is_paid_user(rec.memberid)) {
		printf("member is not paid.\n");
		return;
	}

    rec.id=get_next_issuerecord_id();
    fp=fopen(ISSUERECORD_DB,"ab");
    if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}
    fwrite(&rec, sizeof(ISSUERECORD_T), 1, fp);
    fclose(fp);
    book_changestatus(rec.copyid, STATUS_ISSUED);
}
void book_changestatus(int bookcopy_id, char status[])
{
    BOOKCOPY_T c;
    FILE *fp;
    int size = sizeof(BOOKCOPY_T);
    fp=fopen(BOOKCOPY_DB,"rb+");
    if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}
    while(fread(&c,size,1,fp)>0)
    {
        if(bookcopy_id==c.id)
        {
            strcpy(c.status, status);
            fseek(fp,-size, SEEK_CUR);
            fwrite(&c,size,1,fp);
            break;
        }
    }
    fclose(fp);
}
void display_issued_bookcopies(int member_id)
{
    FILE *fp;
    ISSUERECORD_T rec;
    fp=fopen(ISSUERECORD_DB,"rb");
    if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec,sizeof(ISSUERECORD_T),1,fp)>0)
    {
        if(rec.memberid==member_id && rec.return_date.dd==0)
        issuerecord_display(&rec);
    }
    fclose(fp);
}
void bookcopy_return()
{
    int member_id, record_id;
    FILE *fp;
    ISSUERECORD_T rec;
    printf("Enter memberid: ");
    scanf("%d",&member_id);
    display_issued_bookcopies(member_id);
    int diff_days, found=0;
    int size=sizeof(ISSUERECORD_T);
    printf("Enter issuerecord id which is to be returned: ");
    scanf("%d",&record_id);
    fp=fopen(ISSUERECORD_DB,"rb+");
    if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec,size,1,fp)>0)
    {
        if(rec.id==record_id)
        {
            found=1;
            rec.return_date=date_current();
            diff_days=compare_dates(rec.return_date,rec.return_duedate);
            if(diff_days>0)
            {
            rec.fine_amount=diff_days*FINE_PER_DAY;
            fine_payment(rec.memberid, rec.fine_amount);
            printf("Fine of Rs. %.2lf paid\n",rec.fine_amount);
            }
            break;
        }

    }
    if(found)
    {
        fseek(fp,-size,SEEK_CUR);
        fwrite(&rec,size,1,fp);
        printf("Issue record updated after returning book: \n");
        issuerecord_display(&rec);
        book_changestatus(rec.copyid,STATUS_AVAIL);
    }
    else
    {
        printf("No matching issue record found!!\n");
    }
    
    fclose(fp);       
}
void change_rack()
{
    int copy_id,found=0;
    FILE *fp;
    BOOKCOPY_T rec;
    printf("Enter the Copy id whose rack has to be changed: ");
    scanf("%d",&copy_id);

    fp=fopen(BOOKCOPY_DB,"rb+");
    if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec,sizeof(BOOKCOPY_T),1,fp)>0)
    {
        if(rec.id==copy_id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        int rack;
        printf("Enter the rack number to which the copy is to be transferred: ");
        scanf("%d",&rack);
        rec.rack=rack;
        fseek(fp,-sizeof(BOOKCOPY_T),SEEK_CUR);
        fwrite(&rec, sizeof(BOOKCOPY_T),1, fp);
        printf("Rack changed successfully\n");
        bookcopy_display(&rec);
    }
    else 
    printf("Bookcopy not found!!\n");
    fclose(fp);
}
void list_issued_books()
{
    FILE *fp;
    int flag=0;
    ISSUERECORD_T rec;
    fp=fopen(ISSUERECORD_DB,"rb");
    if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec, sizeof(ISSUERECORD_T),1,fp)>0)
    {
        issuerecord_display(&rec);
        flag=1;
    }
    if(flag==0)
    printf("No books issued.\n");
    fclose(fp);
}
void list_all_members()
{
    USER_T u;
    FILE *fp;
    fp=fopen(USER_DB,"rb");
     if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&u,sizeof(USER_T),1,fp)>0)
    user_display(&u);
    fclose(fp);
}
void take_payment()
{
    PAYMENT_T p;
    FILE *fp;
    strcpy(p.type,PAY_TYPE_FEES);
    p.id=get_next_payment_id();
    payment_accept(&p);
    fp=fopen(PAYMENT_DB,"ab");
    if(fp==NULL) {
		perror("cannot open payment record file");
		return;
	}
  fwrite(&p,sizeof(PAYMENT_T),1,fp);
 
  fclose(fp);
  
}
int is_paid_user(int memberid) {
	DATE now = date_current();
	FILE *fp;
	PAYMENT_T pay;
	int paid = 0;
	// open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	// read payment one by one till eof
	while(fread(&pay, sizeof(PAYMENT_T), 1, fp) > 0) {
		if(pay.userid == memberid && pay.nextpayment_duedate.dd != 0 && compare_dates(now, pay.nextpayment_duedate) < 0) {
			paid = 1;
			break;
		}
	}
	// close file	
	fclose(fp);
	return paid;
}
void fine_payment(int memberid, double fine_amount) {
	FILE *fp;
	PAYMENT_T pay;
	pay.id = get_next_payment_id();
	pay.userid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.transaction_time = date_current();
	memset(&pay.nextpayment_duedate, 0, sizeof(DATE));
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	fwrite(&pay, sizeof(PAYMENT_T), 1, fp);
	fclose(fp);
}
