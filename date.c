#include <stdio.h>
#include "date.h"
#include <time.h>
int is_leap(int yyyy)
{
    if(yyyy%4==0)
    {
        if(yyyy%100==0)
        {
            if(yyyy%400==0)
                return 1;
            else
                return 0;
        }
        else
            return 1;
    }
    else
        return 0;
}
DATE increment_date(DATE d,int n)
{
    DATE res;
    int dd=d.dd + n;
here:
    switch(d.mm)
    {

    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
        if(dd>31)
        {
            dd-=31;
            d.mm+=1;
            if(d.mm>12)
            {
                d.mm-=12;
                d.yyyy+=1;

            }
            goto here;
        }
        break;
    case 12:
        if(dd>31)
        {
            dd-=31;
            d.mm+=1;
            if(d.mm>12)
            {
                d.mm-=12;
                d.yyyy+=1;
            }
            goto here;
        }
        break;
    case 2:
        if(is_leap(d.yyyy))
        {
            if(dd>29)
            {
                dd-=29;
                d.mm+=1;

            }
        }
        else
        {
            if(dd>28)
            {
                dd-=28;
                d.mm+=1;

            }

        }
        goto here;
        break;
    case 4:
    case 6:
    case 9:
    case 11:
        if(dd>30)
        {
            dd-=30;
            d.mm+=1;
            if(d.mm>12)
            {
                d.mm-=12;
                d.yyyy+=1;

            }
            goto here;
        }
        break;
    }
    d.dd=dd;
    res.dd=d.dd;res.mm=d.mm;res.yyyy=d.yyyy;
    return res;
}
void print_date(DATE *d)
{
    printf("%d/%d/%d\n",d->dd,d->mm,d->yyyy);
}
void input_date(DATE *d)
{
    printf("Enter the day\n");
    scanf("%d",&d->dd);
    printf("Enter the month\n");
    scanf("%d",&d->mm);
    printf("Enter the year\n");
    scanf("%d",&d->yyyy);
}
int month_days(int month, int year) {
	int mon_days[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	if(is_leap(year))
		mon_days[2] = 29;
	return mon_days[month];
}
int date_equal(DATE d1, DATE d2) {
	if(d1.dd == d2.dd && d1.mm == d2.mm && d1.yyyy == d2.yyyy)
		return 1;
	return 0;
}

int date_greater(DATE d1, DATE d2) {
	if(d1.yyyy > d2.yyyy || (d1.yyyy == d2.yyyy && d1.mm > d2.mm) || (d1.yyyy == d2.yyyy && d1.mm == d2.mm && d1.dd > d2.dd))
		return 1;
	return 0;
}
int compare_dates(DATE d1,DATE d2)
{
int diff = -1, days = 0;
	DATE min = d1, max = d2;
	if(date_greater(d1, d2)) {
		diff = 1;
		min = d2;
		max = d1;
	}
	while(!date_equal(min, max)) {
		days = days + 1;
		min.dd = min.dd + 1;
		if(min.dd > month_days(min.mm, min.yyyy)) {
			min.dd = 1;
			min.mm = min.mm + 1;
		}
		if(min.mm > 12) {
			min.dd = 1;
			min.mm = 1;
			min.yyyy = min.yyyy + 1;
		}
	}
	return diff * days;
}
DATE date_current() {
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	DATE now;
	now.dd = tm->tm_mday;
	now.mm = tm->tm_mon + 1;
	now.yyyy = tm->tm_year + 1900;
	return now;
}
