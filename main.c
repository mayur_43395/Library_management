#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"

void test_date()
{
    DATE d1,d2;
    input_date(&d1);
    print_date(&d1);
    input_date(&d2);
    print_date(&d2);
    //increment_date(&d1,119);
    int n=compare_dates(d1,d2);
    printf("%d",n);
}

int main()
{
    /*USER_T u;
owner_area(&u);
librarian_area(&u);
member_area(&u);*/
int choice;
	do {
		printf("\n\n0-Exit\n1-Sign In\n2-Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: 
			sign_in();
			break;
		case 2:
			sign_up();	
			break;
		}
	}while(choice != 0);

    return 0;
}
