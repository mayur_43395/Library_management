#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(USER_T *u)
{
    int choice;
    do{
    printf("\n\n0-Sign Out\n1-Appoint librarian\n2-Edit Profile\n3-Edit Password\n4-Fees report\n5-Fine Report\n6-Book Availabilty\n7-Book Categories\nEnter choice: \n");
    scanf("%d",&choice);
    switch(choice)
    {
        case 1:appoint_librarian();break;
        case 2:edit_profile(u); break;
        case 3:change_password(u);break;
        case 4:fees_report();break;
        case 5:fine_report();break;
        case 6:bookcopy_checkavail_details();break;
        case 7:book_category();break;
        
    }
    }while(choice!=0);
    
}
void appoint_librarian()
{
    USER_T u;
    u.id=get_next_user_id();
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	user_add(&u);

}
void fees_report()
{
    FILE *fp;
    PAYMENT_T p;
    fp=fopen(PAYMENT_DB,"rb");
    double fees=0;
    DATE d1,d2;
    printf("enter the intial date: \n");
    input_date(&d1);
    printf("enter the final date: ");
    input_date(&d2);
    if(fp == NULL) {
		perror("cannot open payment record file");
		exit(1);
	
    }
    while(fread(&p,sizeof(PAYMENT_T),1,fp)>0)
    {
        if(compare_dates(p.transaction_time,d2)<=0)
        if(strcmp(p.type,PAY_TYPE_FEES)==0)
        fees+=p.amount;
    }
    fclose(fp);
    printf("from date : ");
    print_date(&d1);
    printf("to date : ");
    print_date(&d2);
    printf("Fees : %0.2lf\n",fees);
}
void book_category()
{
    BOOK_T b;
    BOOKCOPY_T c;
    FILE *f, *x;
    f=fopen(BOOK_DB,"rb");
    if(f==NULL)
    {
        perror("File cannot be opened\n");
        return;
    }
    x=fopen(BOOKCOPY_DB,"rb");
    if(x==NULL)
    {
        perror("File cannot be opened\n");
        return;
    }
    while(fread(&b,sizeof(BOOK_T),1,f)>0)
    {
        book_display(&b);
        while(fread(&c, sizeof(BOOKCOPY_T),1,x)>0)
        {
            if(b.id==c.bookid)
            bookcopy_display(&c);
        }
    }
  fclose(f);
  fclose(x);
}
void fine_report()
{
    FILE *fp;
    PAYMENT_T p;
    fp=fopen(PAYMENT_DB,"rb");
    double fine=0;
    DATE d1,d2;
    printf("enter the intial date: \n");
    input_date(&d1);
    printf("enter the final date: ");
    input_date(&d2);
    if(fp == NULL) {
		perror("cannot open payment record file");
		exit(1);
	
    }
    while(fread(&p,sizeof(PAYMENT_T),1,fp)>0)
    {
        if(compare_dates(p.transaction_time,d2)<=0)
        if(strcmp(p.type,PAY_TYPE_FINE)==0)
        fine+=p.amount;
    }
    fclose(fp);
    printf("from date : ");
    print_date(&d1);
    printf("to date : ");
    print_date(&d2);
    printf("Fees : %0.2lf\n",fine);
}