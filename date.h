#ifndef DATE_H_INCLUDED
#define DATE_H_INCLUDED
#include <time.h>
typedef struct date
{
    int dd;
    int mm;
    int yyyy;
} DATE;
int is_leap(int yyj);
DATE increment_date(DATE d,int n);
DATE date_current();
void print_date(DATE *d);
void input_date(DATE *d);
int compare_dates(DATE d1,DATE d2);
int date_greater(DATE d1, DATE d2);
int date_equal(DATE d1, DATE d2);
int month_days(int month, int year);


#endif // DATE_H_INCLUDED
